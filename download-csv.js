var blob = new Blob([csvData]);
if (window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveBlob(blob, 'data.csv');
} else {
    var a = window.document.createElement('a');
    a.href = window.URL.createObjectURL(blob, { type: 'text/plain' });
    a.download = 'data.csv';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a)
}